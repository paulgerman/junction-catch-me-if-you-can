
var express = require('express');



var router = express.Router();
var python_runner = require("./python-runner.js");
var fs = require('fs');

router.get('/', function (req, res, next) {
    var all = [];

    all.push(python_runner.runScript("vulnerabilities", "vulnerabilities.py", ""));

    Promise.all(all).then(function (values) {

        for (i in values) {
            values[i]["results"] = JSON.parse(values[i]["results"][0]);
        }


        console.log(values);

        res.render("main", {result: values});
    });
});

router.get('/search', function (req, res, next) {

    var query = req.query["query"];

    var all = [];

    all.push(python_runner.runScript("vuldb", "vuldb.py", query));
    all.push(python_runner.runScript("circl", "circl.py", query));
    all.push(python_runner.runScript("ibm", "ibm-search.py", query));

    //add other keywords!
    all.push(python_runner.runScript("twitter-search", "twitter-search.py", query));

    Promise.all(all).then(function (values) {

        for (i in values) {
            values[i]["results"] = JSON.parse(values[i]["results"][0]);
        }

        var vuldb = values.find(item => item.source == "vuldb")["results"];
        var circl = values.find(item => item.source == "circl")["results"];

        var ibm = values.find(item => item.source == "ibm")["results"];
        var twitter_search = values.find(item => item.source == "twitter-search")["results"];

        var time = vuldb["timeline"];
        var processedData = {};

        for(var thing in time) {
            var date = new Date(time[thing]["date"] * 1000);
            var str = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDay();
            processedData[str] = processedData.hasOwnProperty(str) ? processedData[str] + 1 : 1;
        }

        vuldb["timeline"] = processedData;

        var data = {
            vuldb: vuldb,
            circl: circl,
            twittersearch: twitter_search,
            popularity: vuldb.risk_val,
            level: vuldb.risk,
            query: query,
            ibm: ibm
        };

        res.render('result', data);
    });

});

module.exports = router;