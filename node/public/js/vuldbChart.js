Chart.defaults.global.defaultFontColor = '#292b2c';

data = data["timeline"];
var ctx = document.getElementById("myAreaChart");
var finalArr = [];

console.log(data);
Object.keys(data).forEach(function(key){
    finalArr.push({x: new Date(key), y: data[key]});
});

var config = {
    type:    'line',
    data:    {
        datasets: [
            {
                label: "the thing",
                data:  finalArr,
                fill:  false,
                borderColor: 'blue'
            }
        ]
    },
    options: {
        responsive: true,
        scales:     {
            xAxes: [{
                type:       "time",
                time:       {
                    tooltipFormat: 'll'
                },
                scaleLabel: {
                    display:     true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display:     true,
                    labelString: 'value'
                }
            }]
        }
    }
};

var  myLineChart = new Chart(ctx, config);
