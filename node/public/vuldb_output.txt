{
    "response": {
        "version": "3.5",
        "format": "json",
        "status": "200",
        "lang": "en",
        "items": 1,
        "consumption": 1,
        "remaining": 47,
        "querylimit": 10,
        "timestamp": "1543064034",
        "rtt": 2,
        "etag": "3081733efa679484-82587b1b6c80586b-dcca48101505dd86"
    },
    "request": {
        "timestamp": "1543064032",
        "apikey": "valid",
        "myfilter": "0",
        "userid": "1865",
        "details": 1,
        "sort": "entry_timestamp_create",
        "type": "search",
        "value": "CVE-2014-6271"
    },
    "result": [
        {
            "entry": {
                "id": "67685",
                "title": "GNU Bash up to 3.2.48 Environment Variable variables.c privilege escalation",
                "summary": "A vulnerability was found in GNU Bash and classified as very critical. This issue affects an unknown function of the file variables.c of the component Environment Variable Handler.  Applying a patch is able to eliminate this problem. The bugfix is ready for download at bugzilla.novell.com. The problem might be mitigated by replacing the product with Shell as an alternative. The best possible mitigation is suggested to be patching the affected component. A possible mitigation has been published even before and not after the disclosure of the vulnerability. Red Hat posted several mod_security rules which help to prevent exploitation of this vulnerability. It is also possible to enforce such a limitation with an IPTables rule: \"iptables using -m string --hex-string '|28 29 20 7B|'\" Attack attempts may be identified with Snort ID 31975. In this case the pattern () { is used for detection.  Furthermore it is possible to detect and prevent this kind of attack with TippingPoint and the filter 16800. ",
                "timestamp": {
                    "create": "1411593255",
                    "change": "1530279275"
                }
            },
            "software": {
                "vendor": "GNU",
                "name": "Bash",
                "version": [
                    "1.14.0",
                    "1.14.1",
                    "1.14.2",
                    "1.14.3",
                    "1.14.4",
                    "1.14.5",
                    "1.14.6",
                    "1.14.7",
                    "2.0",
                    "2.01",
                    "2.01.1",
                    "2.02",
                    "2.02.1",
                    "2.03",
                    "2.04",
                    "2.05",
                    "3.0",
                    "3.0.16",
                    "3.1",
                    "3.2",
                    "3.2.48"
                ],
                "component": "Environment Variable Handler",
                "file": "variables.c",
                "argument": "Environment",
                "cpe": [
                    "cpe:\/a:gnu:bash:1.14.0",
                    "cpe:\/a:gnu:bash:1.14.1",
                    "cpe:\/a:gnu:bash:1.14.2",
                    "cpe:\/a:gnu:bash:1.14.3",
                    "cpe:\/a:gnu:bash:1.14.4",
                    "cpe:\/a:gnu:bash:1.14.5",
                    "cpe:\/a:gnu:bash:1.14.6",
                    "cpe:\/a:gnu:bash:1.14.7",
                    "cpe:\/a:gnu:bash:2.0",
                    "cpe:\/a:gnu:bash:2.01",
                    "cpe:\/a:gnu:bash:2.01.1",
                    "cpe:\/a:gnu:bash:2.02",
                    "cpe:\/a:gnu:bash:2.02.1",
                    "cpe:\/a:gnu:bash:2.03",
                    "cpe:\/a:gnu:bash:2.04",
                    "cpe:\/a:gnu:bash:2.05",
                    "cpe:\/a:gnu:bash:3.0",
                    "cpe:\/a:gnu:bash:3.0.16",
                    "cpe:\/a:gnu:bash:3.1",
                    "cpe:\/a:gnu:bash:3.2",
                    "cpe:\/a:gnu:bash:3.2.48"
                ],
                "affectedlist": [
                    "Apple iPhone (Jailbreak only)\r",
                    "Apple Mac OS X bis 10.9.4\r",
                    "Debian GNU\/Linux bis 4.1-3\/4.2\r",
                    "F5 BIG-IP bis 11.6.0\r",
                    "Madravia Linux 1.0\r",
                    "Palo Alto PAN-OS bis 6.0\r",
                    "Red Hat Linux 4\/5\/6\/7\r",
                    "Slackware Linux bis 14.1\r",
                    "SuSE openSUSE 11.0\r",
                    "Ubuntu Linux bis 14.04 LTS\r",
                    "VMware Fusion"
                ],
                "notaffectedlist": [
                    "Android Default Installation\r",
                    "FreeBSD Default Installation\r",
                    "NetBSD Default Installation\r",
                    "OpenBSD Default Installation"
                ]
            },
            "vulnerability": {
                "risk": {
                    "value": "3",
                    "name": "high"
                },
                "class": "privilege escalation",
                "cwe": "CWE-78",
                "cvss2": {
                    "vuldb": {
                        "basescore": "9.3",
                        "tempscore": "7.3",
                        "av": "N",
                        "ac": "M",
                        "au": "N",
                        "ci": "C",
                        "ii": "C",
                        "ai": "C",
                        "e": "POC",
                        "rl": "OF",
                        "rc": "C"
                    }
                },
                "cvss3": {
                    "meta": {
                        "basescore": "9.8",
                        "tempscore": "8.8"
                    },
                    "vuldb": {
                        "basescore": "9.8",
                        "tempscore": "8.8",
                        "av": "N",
                        "ac": "L",
                        "pr": "N",
                        "ui": "N",
                        "s": "U",
                        "c": "H",
                        "i": "H",
                        "a": "H",
                        "e": "POC",
                        "rl": "OF",
                        "rc": "C"
                    }
                },
                "titleword": "Shellshock",
                "advisoryquote": "(...) a vulnerability in bash, related to how environment variables are processed: trailing code in function definitions was executed, independent of the variable name.",
                "timeline": [
                    {
                        "date": "1410220800",
                        "event": "CVE assigned",
                        "link": "https:\/\/cve.mitre.org\/cgi-bin\/cvename.cgi?name=CVE-2014-6271",
                        "color": "blue"
                    },
                    {
                        "date": "1410912000",
                        "event": "Countermeasure disclosed",
                        "diff": "+8 days",
                        "link": "https:\/\/bugzilla.novell.com\/attachment.cgi?id=606672&action=edit",
                        "color": "blue"
                    },
                    {
                        "date": "1411516800",
                        "event": "Advisory disclosed",
                        "diff": "+7 days",
                        "link": "http:\/\/seclists.org\/oss-sec\/2014\/q3\/649",
                        "color": "blue"
                    },
                    {
                        "date": "1411516800",
                        "event": "Exploit disclosed",
                        "diff": "+0 days",
                        "link": "https:\/\/securityblog.redhat.com\/2014\/09\/24\/bash-specially-crafted-environment-variables-code-injection-attack\/",
                        "color": "red"
                    },
                    {
                        "date": "1411516800",
                        "event": "EDB entry disclosed",
                        "diff": "+0 days",
                        "link": "https:\/\/www.exploit-db.com\/exploits\/34765",
                        "color": "red"
                    },
                    {
                        "date": "1411516800",
                        "event": "VulDB entry created",
                        "diff": "+0 days",
                        "link": "https:\/\/vuldb.com\/?id.67685",
                        "color": "blue"
                    },
                    {
                        "date": "1411516800",
                        "event": "NVD disclosed",
                        "diff": "+0 days",
                        "link": "https:\/\/cve.mitre.org\/cgi-bin\/cvename.cgi?name=CVE-2014-6271",
                        "color": "blue"
                    },
                    {
                        "date": "1411516800",
                        "event": "SecurityTracker entry created",
                        "diff": "+0 days",
                        "link": "https:\/\/www.securitytracker.com\/id\/1030890",
                        "color": "blue"
                    },
                    {
                        "date": "1411516800",
                        "event": "VulnerabilityCenter entry assigned",
                        "diff": "+0 days",
                        "link": "https:\/\/www.vulnerabilitycenter.com\/#!vul=57351",
                        "color": "blue"
                    },
                    {
                        "date": "1411603200",
                        "event": "SecurityFocus entry assigned",
                        "diff": "+1 days",
                        "link": "https:\/\/www.securityfocus.com\/bid\/70137",
                        "color": "blue"
                    },
                    {
                        "date": "1411603200",
                        "event": "Secunia entry created",
                        "diff": "+0 days",
                        "link": "https:\/\/secuniaresearch.flexerasoftware.com\/advisories\/61541",
                        "color": "blue"
                    },
                    {
                        "date": "1411948800",
                        "event": "Nessus plugin released",
                        "diff": "+4 days",
                        "link": "https:\/\/www.tenable.com\/plugins\/nessus\/77970",
                        "color": "blue"
                    },
                    {
                        "date": "1458086400",
                        "event": "VulnerabilityCenter entry created",
                        "diff": "+534 days",
                        "link": "https:\/\/www.vulnerabilitycenter.com\/#!vul=57351",
                        "color": "blue"
                    },
                    {
                        "date": "1466380800",
                        "event": "VulnerabilityCenter entry updated",
                        "diff": "+96 days",
                        "link": "https:\/\/www.vulnerabilitycenter.com\/#!vul=57351",
                        "color": "blue"
                    },
                    {
                        "date": "1530279275",
                        "event": "VulDB last update",
                        "diff": "+739 days",
                        "link": "https:\/\/vuldb.com\/?id.67685",
                        "color": "blue"
                    }
                ]
            },
            "advisory": {
                "date": "1411516800",
                "url": "http:\/\/seclists.org\/oss-sec\/2014\/q3\/649",
                "reportconfidence": "confirmed",
                "person": {
                    "name": "Stephane Chazelas"
                },
                "advisoryquote": "Chet Ramey, the GNU bash upstream maintainer, will soon release official upstream patches."
            },
            "exploit": {
                "availability": "1",
                "date": "1411516800",
                "publicity": "public",
                "url": "https:\/\/securityblog.redhat.com\/2014\/09\/24\/bash-specially-crafted-environment-variables-code-injection-attack\/",
                "developer": {
                    "name": "Huzaifa Sidhpurwala"
                },
                "language": "Bash",
                "exploitability": "proof-of-concept",
                "price": {
                    "0day": "$100k and more",
                    "today": "$0-$5k"
                }
            },
            "countermeasure": {
                "remediationlevel": "official fix",
                "name": "Patch",
                "date": "1410912000",
                "patch": {
                    "url": "https:\/\/bugzilla.novell.com\/attachment.cgi?id=606672&action=edit"
                },
                "alternative": {
                    "name": "Shell"
                }
            },
            "source": {
                "cve": {
                    "id": "CVE-2014-6271",
                    "assigned": "1410220800",
                    "published": "1411516800",
                    "summary": "GNU Bash through 4.3 processes trailing strings after function definitions in the values of environment variables, which allows remote attackers to execute arbitrary code via a crafted environment, as demonstrated by vectors involving the ForceCommand feature in OpenSSH sshd, the mod_cgi and mod_cgid modules in the Apache HTTP Server, scripts executed by unspecified DHCP clients, and other situations in which setting the environment occurs across a privilege boundary from Bash execution, aka \"ShellShock.\" NOTE: the original fix for this issue was incorrect; CVE-2014-7169 has been assigned to cover the vulnerability that is still present after the incorrect fix."
                },
                "oval": {
                    "id": "oval:org.mitre.oval:def:28331"
                },
                "iavm": {
                    "id": "2014-A-0142",
                    "vmskey": "V0054753",
                    "title": "GNU Bash Shell Code Execution Vulnerability"
                },
                "osvdb": {
                    "id": "112004",
                    "title": "GNU bash Environment Variable Handling Shell Command Injection"
                },
                "secunia": {
                    "id": "61541",
                    "date": "1411603200",
                    "title": "GNU Bash Shell Function Definitions OS Commands Injection Vulnerability"
                },
                "securityfocus": {
                    "id": "70137",
                    "date": "1411603200",
                    "title": "GNU Bash CVE-2014-7169 Incomplete Fix Remote Code Execution Vulnerability",
                    "class": "Design Error"
                },
                "sectracker": {
                    "id": "1030890",
                    "title": "GNU bash Environment Variable Processing Flaw Lets Users Execute Arbitrary Code"
                },
                "vulnerabilitycenter": {
                    "id": "57351",
                    "creationdate": "1458086400",
                    "lastupdate": "1466380800",
                    "reportingdate": "1411516800",
                    "title": "GNU Bash through 4.3 Remote Code Execution via a Crafted Environment - CVE-2014-6271"
                },
                "xforce": {
                    "id": "96209",
                    "identifier": "bash-cve20147169-command-exec",
                    "title": "GNU Bash variables command execution",
                    "risk": "High Risk"
                },
                "exploitdb": {
                    "id": "34765",
                    "date": "1411516800"
                },
                "heise": {
                    "id": "2403305"
                },
                "nessus": {
                    "id": "77970",
                    "date": "1411948800",
                    "name": "Qmail Remote Command Execution via Shellshock",
                    "filename": "ala_ALAS-2014-418.nasl",
                    "family": "SMTP problems"
                },
                "openvas": {
                    "id": "871250",
                    "title": "RedHat Update for bash RHSA-2014:1306-01",
                    "filename": "gb_RHSA-2014_1306-01_bash.nasl",
                    "family": "Red Hat Local Security Checks"
                },
                "qualys": {
                    "id": "150134"
                },
                "saint": {
                    "id": "exploit_info\/bash_shellshock_cups",
                    "title": "Bash Environment Variable Handling Shell Command Injection Via CUPS",
                    "link": "http:\/\/www.saintcorporation.com\/cgi-bin\/exploit_info\/bash_shellshock_cups"
                },
                "metasploit": {
                    "id": "apache_mod_cgi_bash_env.rb",
                    "title": "Apache ActiveMQ Directory Traversal",
                    "filename": "apache_mod_cgi_bash_env.rb"
                },
                "snort": {
                    "id": "31975",
                    "class": "attempted-admin",
                    "message": "Volex \u2013 Possible CVE-2014-6271 bash Vulnerability Requested (header)",
                    "pattern": "() {"
                },
                "suricata": {
                    "id": "2014092401",
                    "sig": "() {",
                    "class": "attempted-admin"
                },
                "issproventia": {
                    "id": "2132121"
                },
                "tippingpoint": {
                    "id": "16800"
                },
                "mcafeeips": {
                    "id": "SMTP: EXPN Command Used",
                    "version": "8.1.45.5"
                },
                "fortigateips": {
                    "id": "39294"
                },
                "videolink": {
                    "url": "https:\/\/youtu.be\/aa-O5EyCcKc"
                },
                "misc": {
                    "url": "https:\/\/access.redhat.com\/articles\/1200223"
                },
                "seealso": [
                    "71528"
                ]
            }
        }
    ]
}