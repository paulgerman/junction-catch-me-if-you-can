import sys

import requests
import json


apikey = "176780f38ea3efaa6b7e91c13eaa4bb9"

API_ENDPOINT = 'https://vuldb.com/?api'


def search_vuldb(query):
    data = {
        'apikey': apikey,
        'search': query,
        'details': 1
    }

    request = requests.post(url=API_ENDPOINT, data=data)
    return parse_vuldb(json.loads(request.text))


def parse_vuldb(data):
    #f = open("thing.txt", "w")

    #f.write(json.dumps(data))

    if len(data["result"]) == 0:
        return {}

    popularity = 0

    keywords = []

    popularity += len(data["result"][0]["vulnerability"]["timeline"])
    popularity += len(data["result"][0]["source"])

    data["result"][0]["popularity"] = popularity

    if "titleword" in data["result"][0]["vulnerability"]:
        keywords.append(data["result"][0]["vulnerability"]["titleword"])

    affectedSoftware = []

    if "affectedlist" in data["result"][0]["software"]:
        affectedSoftware = data["result"][0]["software"]["affectedlist"]

    vendor = ""
    if "vendor" in data["result"][0]["software"]:
        vendor = data["result"][0]["software"]["vendor"]

    return {
        "popularity": popularity,
        "keywords": keywords,
        "software": data["result"][0]["software"]["name"] + " " + vendor,
        "affectedSoftware": affectedSoftware,
        "affectedSoftware_num": len(affectedSoftware),
        "risk": data["result"][0]["vulnerability"]["risk"]["name"],
        "timeline": data["result"][0]["vulnerability"]["timeline"],
        'risk_val': data["result"][0]["vulnerability"]["risk"]["value"],

    }


print(json.dumps(search_vuldb(sys.argv[1])))
