import json
import sys
from TwitterSearch import *
import time
import pandas as pd

def search_python(query_list):
    try:
        tso = TwitterSearchOrder() # create a TwitterSearchOrder object
        tso.set_keywords(query_list) # let's define all words we would like to have a look for
        tso.set_language('en') # we want to see English tweets only
        tso.set_include_entities(False) # and don't give us all those entity information

        #tso.set_result_type('mixed')
        # it's about time to create a TwitterSearch object with our secret tokens
        ts = TwitterSearch(
            consumer_key='MNNhtt2iAZDRomUlgSboOCeWy',
            consumer_secret='4dKqA3t11F8agJO6EOjbNpxfrabQDkvkVKiToL0GjINOUriqcS',
            access_token='601212044-7gxYsEHVoqWFnkAdGArCewSJZkmOiui4HtMWWFJG',
            access_token_secret='A6sOJ2KkcIbbvX6qJiiakbmqmiJqOMVEhRwi3PvcrA8js'
        )

        tweets = []

        # this is where the fun actually starts :)
        for tweet in ts.search_tweets_iterable(tso):
            tweets.append({
                "username": tweet['user']['screen_name'],
                "text": tweet['text'],
                "created_at": time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')),
                "url": "https://twitter.com/-/status/" + tweet["id_str"]
            })
            
        return tweets

    except TwitterSearchException as e: # take care of all those ugly errors if there are some
        print(e)

def count_tweets(tweets):
    
    dates = []
    
    for d in tweets:
       dates.append(d.get('created_at'))
        
    idx = pd.to_datetime(dates)
    new = pd.Series(idx.to_period('d'))
    df = new.value_counts().sort_index().reset_index()
    df.columns = ['Date','Count']
    df.Date = df.Date.dt.strftime('%Y-%m-%d')
    
    return df

print(json.dumps(search_python(sys.argv[1:])))
