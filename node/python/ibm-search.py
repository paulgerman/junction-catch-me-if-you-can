# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 21:12:01 2018

@author: Patty
"""
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
import pandas as pd
import numpy as np
import time
import json
import os
import sys

# =============================================================================
# Requirements: Selenium WebDriver, geckodriver
# =============================================================================
def search_IBM_blog(keywords):
    options = Options()
    options.add_argument("--headless")
    
    #geckopath has to be in same working directory
    driver = webdriver.Firefox(firefox_options=options)

    url ="https://www.ibm.com/blogs/psirt/"    
    driver.get(url)

    titles = list()
    dates = list()
    level = list()
    urls = list()
    
    driver.find_element_by_id("q").send_keys(keywords)

    ##while(True):

        #Get the titles
    for t in driver.find_elements_by_xpath('.//span[@class = "ibm-textcolor-default"]'):
        titles.append(t.text)
        
    #Get the date
    for t in driver.find_elements_by_xpath('.//h4[@class="ibm-medium"]'):
        if(len(t.text.split("|"))>1):   
            dates.append(t.text.split("|")[0])
            level.append(t.text.split("|")[1])
            
        else: 
            dates.append(t.text)
            level.append("not specified")
    
    for t in driver.find_elements_by_xpath('.//p[@class="ibm-medium ibm-padding-bottom-1"]/a'):
        urls.append(t.get_attribute('href'))


#        try:
#           driver.find_elements_by_xpath(".//a[@class='next page-numbers ibm-next-link ibm-inlinelink ibm-icon-after']")[0].click()
#        except NoSuchElementException:
   
    driver.close()
    data = []

        # this is where the fun actually starts :)
    for i in range(0,len(urls)):
        data.append({
            "username": "IBM",
            "text": titles[i]+level[i],
            "created_at": dates[i],
            "url": urls[i]
        })
    
    return data


print(json.dumps(search_IBM_blog(sys.argv[1:])))
