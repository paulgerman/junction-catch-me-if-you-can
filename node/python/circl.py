import sys

import requests
import json

API_ENDPOINT = "http://cve.circl.lu/api/cve/"


def search_circl(search):
    r = requests.get(API_ENDPOINT + search)
    if r == "null":
        return {}
    return parse_circl(json.loads(r.text))


def parse_circl(data):
    if data is None:
        return {
            "exploits_num": 0,
            "scenarios_num": 0,
            "solutions_num": 0,
        }
    num_exploits = 0
    exploits = []
    solutions = []
    scenarios = []

    if "metasploit" in data:
        exploits += data["metasploit"]

    if "exploit-db" in data:
        exploits += data["exploit-db"]

    if "packetstorm" in data:
        exploits += data["packetstorm"]

    if "nessus" in data:
        solutions += data["nessus"]

    if "capec" in data:
        scenarios += data["capec"]

    return {
        "summary": data["summary"],
        "score": data['cvss'],
        "published": data["Published"],
        "modified": data["Modified"],
        "references": data["references"],
        "exploits_num": str(len(exploits)),
        "exploits": exploits,
        "solutions": solutions,
        "solutions_num": str(len(solutions)),
        "scenarios": scenarios,
        "scenarios_num": str(len(scenarios)),
    }


print(json.dumps(search_circl(sys.argv[1])))
