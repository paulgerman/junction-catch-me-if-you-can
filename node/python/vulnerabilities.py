import sys

import requests
import json

apikey = "176780f38ea3efaa6b7e91c13eaa4bb9"

API_ENDPOINT = 'https://vuldb.com/?api'


def search_vuldb():
    data = {
        'apikey': apikey,
        'recent': 30
    }

    request = requests.post(url=API_ENDPOINT, data=data)
    return parse_vul(json.loads(request.text))


def parse_vul(data):
    data = data["result"]
    result = []
    for el in data:
        if "source" in el and "cve" in el["source"]:
            result.append({
                'id': el["source"]["cve"]["id"],
                'title': el["entry"]["title"],
                'risk': el["vulnerability"]["risk"]["name"],
                'risk_val': el["vulnerability"]["risk"]["value"],
                'url': 'https://vuldb.com/?id.' + el["entry"]["id"],
            })

    return result


print(json.dumps(search_vuldb()))
