from collections import Counter
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
# used for removing non-words from the text
regex = re.compile('[^a-zA-Z]')
# Takes in a string, removes stopwords and non-words
def process(doc):
    example_sent = str(doc.read()).lower()
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(example_sent)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    filtered = regex.sub(' ', str(filtered_sentence))
    split_it = filtered.split()
    # Pass the split_it list to instance of Counter class.
    counter = Counter(split_it)
    # most_common() produces k frequently encountered
    # list of most frequent words
    most_occur = counter.most_common(4)
    keywords = ''
    for word in most_occur:
        keywords = keywords +' '+ word[0]
    return keywords







