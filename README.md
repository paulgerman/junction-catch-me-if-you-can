# junction-catch-me-if-you-can

Junction Security Analysis Challenge

The Problem
There are countless vulnerabilities affecting all kinds of software and the sheer volume makes it extremely difficult to effectively identify the threat level and whether it is trending or not. A security analyst right now has to go through multiple structured databases to find relevant matches for CVE (vulnerability) codes and then make sense of this huge data in a very short amount of time. In addition to this, the analyst also searches social media channels like Twitter using the CVE codes or what they think could be possible and most relevant keywords. Social media is already an ocean from which identifying top trends and finding out valuable information in a shot time frame is not possible with a manual approach.
The Solution
Catch Me If You Can (CMIYC) is a platform that solves this problem. The user (the security analyst) enters the platform through the main page where they can 1) Search a CVE number they already know, and 2) Browse through a list of the latest 20 CVEs gathered from multiple structured databases. Whether they want to search an already known CVE or select one of the latest CVEs displayed in the table, the user is lead to the "Results" page. 
Here, the user sees the CVE number, the threat level and the popularity at the top. The popularity denotes how many databases have information on this CVE. Below this basic information are four buttons denoting the main kinds of information gathered from the databases:
-Scenarios: which fields and/or applications this vulnerability applies to.
-Exploits: code that exploits the vulnerability
-Affected Software: List of affected software
-Solutions: how to fix the vulnerabilities (either workarounds or actual solutions)
Clicking on these buttons pops up the relevant lists which the security analyst can browse easily.
Below is a timeline chart of these CVEs. Next is the data gathered from social media. Relevant tweets and reddit data is displayed in tables. A chart shows the per day no. of tweets.

![Screenshot](imgs/screencapture-localhost-3000-search-2018-11-25-12_04_43.png)
![Screenshot](imgs/screencapture-localhost-3000-search-2018-11-25-12_05_37.png)
![Screenshot](imgs/screencapture-localhost-3000-search-2018-11-25-12_05_59.png)
![Screenshot](imgs/screencapture-localhost-3000-search-2018-11-25-12_06_20.png)
![Screenshot](imgs/screencapture-localhost-3000-search-2018-11-25-12_22_13.png)
![Screenshot](imgs/photo_2018-11-26_21-08-03.jpg)
